# Use uma imagem base que inclua um servidor web, como nginx, Apache, etc.
FROM nginx

# Copie os arquivos do seu site para o diretório padrão do servidor web
COPY . /usr/share/nginx/html
EXPOSE 80